#!/usr/bin/env python
# coding: utf-8

# In[2]:


# Second and third screening model where we will just ask PHQ-9 questionaire


#importing libraries
import pandas as pd
import random
from IPython import get_ipython
from PIL import Image
import requests
from io import BytesIO

ipy = get_ipython()
if ipy is not None:
    ipy.run_line_magic('matplotlib', 'inline')
    ipy.run_line_magic('pylab', 'inline')

#importing images

# phq9_none_img = mpimg.imread('PHQ9_none.jpg')
# phq9_mild_img = mpimg.imread('PHQ9_mild.jpg')
# phq9_moderate_img = mpimg.imread('PHQ9_moderate.jpg')
# phq9_moderatelysevere_img = mpimg.imread('PHQ9_moderatelysevere.jpg')
# phq9_severe_img = mpimg.imread('PHQ9_severe.jpg')



#creating user-defined function

def main():

    #fetching data from database
    therapist_names=("Sarah","Mindy")
    phq9score = int( input ("PHQ-9 score: "))
    

#PHQ9
    

    if 0<= phq9score <= 4:
        phq9 = "None" 
        phq9_treatment = "None"
        response = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb')
        phq9_img = Image.open(BytesIO(response.content))

    elif 5<= phq9score <= 9:
        phq9 = "Mild"
        phq9_treatment = "Watchful waiting; repeat PHQ-9 at follow-up"
        response = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb')
        phq9_img = Image.open(BytesIO(response.content))

    elif 10<= phq9score <= 14:
        phq9 = "Moderate"
        phq9_treatment = "Treatment plan, considering counseling, follow-up and/or pharmacotherapy"
        response = requests.get(
            'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb')
        phq9_img = Image.open(BytesIO(response.content))

    elif 15<= phq9score <= 19:
        phq9 = "Moderately Severe"
        phq9_treatment = "Active treatment with pharmacotherapy and/or psychotherapy"

        response = requests.get(
            'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb')
        phq9_img = Image.open(BytesIO(response.content))

    else:
        phq9 = "Severe"
        phq9_treatment = "Immediate initiation of pharmacotherapy and, if severe impairment or poor response to therapy, expedited referral to a mental health specialist for psychotherapy and/or collaborative management"


        response = requests.get(
            'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb')
        phq9_img = Image.open(BytesIO(response.content))



        
        
#Therapist 

    if phq9score >= 5:
        therapist = random.choice(therapist_names)
    elif phq9score <= 4 :
        therapist = 'None'

    
    #creating a dataframe for all results
    data = {'PHQ9_Score':[phq9score],'PHQ9_Risk': [phq9],'PHQ9_Treatment_Options': [phq9_treatment], 
            'Therapist':[therapist],'PHQ9_meter':[phq9_img]}

    
    df = pd.DataFrame(data)
    return df



#calling function
df = main()


#saving results to csv file
df.to_csv("risk_2_3_screen.csv", index=False)

