#!/usr/bin/env python
# coding: utf-8

# In[9]:


# First screening model

# Importing libraries
import pandas as pd
import random
import pickle
from IPython import get_ipython
from PIL import Image
import requests
from io import BytesIO

ipy = get_ipython()
if ipy is not None:
    ipy.run_line_magic('matplotlib', 'inline')
    ipy.run_line_magic('pylab', 'inline')

#importing images

ptsd_none_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPTSD_none.JPG?alt=media&token=6e9db2bc-3586-433a-9de0-aa01196cc39e')
ptsd_mild_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPTSD_mild.JPG?alt=media&token=223422d0-040a-432e-8bc0-8e669f5b48dc')
ptsd_moderate_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPTSD_moderate.JPG?alt=media&token=7c09b036-02bc-483a-b993-e83581df616f')
ptsd_severe_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPTSD_severe.JPG?alt=media&token=f401ddf3-6e06-4567-8ee4-70eb0830b41f')

phq9_none_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb')
phq9_mild_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb')
phq9_moderate_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb')
phq9_moderatelysevere_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_moderatelysevere.JPG?alt=media&token=e8ea32d2-4b7f-4ed9-9470-9b8844a0851b')
phq9_severe_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_severe.JPG?alt=media&token=fc8c741a-a3f5-4e95-9079-19e4bda0c6f3')

ace_none_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FACE_none.JPG?alt=media&token=13c7dd04-b25b-49c1-85a3-14dc174dac7d')
ace_moderate_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FACE_moderate.JPG?alt=media&token=cb316679-8418-48b9-923a-f54722c919a3')
ace_severe_img = requests.get('https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FACE_severe.JPG?alt=media&token=d4407bed-7bdd-47be-96dc-58e74ca5eaed')


#Creating user-defined function

def main():

    # Fetching data from database
    therapist_names=("Sarah","Mindy")
    ptsdscore = int( input ("PTSD score: "))
    phq9score = int( input ("PHQ-9 score: "))
    acescore = int( input ("ACE score: "))

    
#PTSD

    if 0<= ptsdscore <= 5:
        PTSD = "None" 
        PTSD_treatment = "None"
        ptsd_img = Image.open(BytesIO(ptsd_none_img.content))
    elif 6<= ptsdscore <= 8:
        PTSD = "Mild"
        PTSD_treatment = "cognitive behavioral therapy (CBT), Prolonged Exposure Therapy, Eye Movement Desensitization and Reprocessing, Stress Inoculation Training"
        ptsd_img = Image.open(BytesIO(ptsd_none_img.content))
    elif 9<= ptsdscore <= 11:
        PTSD = "Moderate"
        PTSD_treatment = "Active treatment with pharmacotherapy and/or psychotherapy (cognitive behavioral therapy (CBT), Prolonged Exposure Therapy, Eye Movement Desensitization and Reprocessing, Stress Inoculation Training)"
        ptsd_img = Image.open(BytesIO(ptsd_moderate_img.content))
    elif 12<= ptsdscore <= 15:
        PTSD = "Severe"
        PTSD_treatment = "Active treatment with pharmacotherapy and/or psychotherapy (cognitive behavioral therapy (CBT), Prolonged Exposure Therapy, Eye Movement Desensitization and Reprocessing, Stress Inoculation Training)"
        ptsd_img = Image.open(BytesIO(ptsd_severe_img.content))

        
#PHQ9
    
    if 0<= phq9score <= 4:
        phq9 = "None" 
        phq9_treatment = "None"
        phq9_img = Image.open(BytesIO(phq9_none_img.content))
    elif 5<= phq9score <= 9:
        phq9 = "Mild"
        phq9_treatment = "Watchful waiting; repeat PHQ-9 at follow-up"
        phq9_img = Image.open(BytesIO(phq9_mild_img.content))
    elif 10<= phq9score <= 14:
        phq9 = "Moderate"
        phq9_treatment = "Treatment plan, considering counseling, follow-up and/or pharmacotherapy"
        phq9_img = Image.open(BytesIO(phq9_moderate_img.content))
    elif 15<= phq9score <= 19:
        phq9 = "Moderately Severe"
        phq9_treatment = "Active treatment with pharmacotherapy and/or psychotherapy"
        phq9_img = Image.open(BytesIO(phq9_moderatelysevere_img.content))
    else:
        phq9 = "Severe"
        phq9_treatment = "Immediate initiation of pharmacotherapy and, if severe impairment or poor response to therapy, expedited referral to a mental health specialist for psychotherapy and/or collaborative management"
        phq9_img = Image.open(BytesIO(phq9_severe_img.content))
        
#ACE
    
    if acescore <= 1:
        ace = "None"
        ace_img = Image.open(BytesIO(ace_none_img.content))
    elif 1 < acescore < 4:
        ace = "Moderate"
        ace_img = Image.open(BytesIO(ace_moderate_img.content))
    elif acescore >= 4:
        ace = "Severe"
        ace_img = Image.open(BytesIO(ace_severe_img.content))
        
        
        
#Therapist 

    if ptsdscore >= 6 or phq9score >= 5 or acescore >= 4:
        therapist = random.choice(therapist_names)
    elif ptsdscore <= 5 and phq9score <= 4 and acescore < 4:
        therapist = 'None'
        
      
    #creating a dataframe in which results will be stored
    data = {'PTSD_Score':[ptsdscore],'PTSD_Risk': [PTSD],'PTSD_Treatment_Options': [PTSD_treatment],'PTSD_meter':[ptsd_img],
            'PHQ9_Score':[phq9score],'PHQ9_Risk': [phq9],'PHQ9_Treatment_Options': [phq9_treatment],'PHQ9_meter':[phq9_img],
            'ACE_Score':[acescore],'Ace_Risk':[ace],'ACE_meter':[ace_img],'Therapist':[therapist]}


    
    df = pd.DataFrame(data)
    return df

#Calling function
df = main()

#Saving results dataframe in a csv file
df.to_csv("risk.csv", index=False)

# Pickle the model
filename = 'first_result.pkl'
pickle.dump(df, open(filename, 'wb'))

# Testing if pickling worked
loaded_model = pickle.load(open(filename, 'rb'))
print(loaded_model)

