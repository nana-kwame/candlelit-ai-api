Flask==1.1.2
imagesize==1.2.0
ipython-genutils==0.2.0
pandas
pathtools==0.1.2
pymongo==3.11.2
gunicorn==19.10.0
ipython<6.0.0