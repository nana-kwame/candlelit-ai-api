# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


# def print_hi(name):
#     # Use a breakpoint in the code line below to debug your script.
#     print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
# if __name__ == '__main__':
#     print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
import os

from flask import Flask
from flask import jsonify, make_response, request
# from flask_restplus import reqparse, abort, Api, Resource
import pandas as pd
import random
from IPython import get_ipython
# from PIL import Image
# import requests
# from io import BytesIO

ipy = get_ipython()
# if ipy is not None:
#     ipy.run_line_magic('matplotlib', 'inline')
#     ipy.run_line_magic('pylab', 'inline')

app = Flask(__name__)


# api = Api(app)


# Argument parsing
# parser = reqparse.RequestParser()
# parser.add_argument('query')


@app.route('/baseline', methods=['POST'])
def main():
    try:
        therapist_names = ('Sarah', 'Mindy')
        filter_params = request.get_json()

        print('Variables', filter_params)

        user_scores = filter_params['query']

        # Importing images
        ptsd_none_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPTSD_none.JPG?alt=media&token=6e9db2bc-3586-433a-9de0-aa01196cc39e'
        ptsd_mild_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPTSD_mild.JPG?alt=media&token=223422d0-040a-432e-8bc0-8e669f5b48dc'
        ptsd_moderate_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPTSD_moderate.JPG?alt=media&token=7c09b036-02bc-483a-b993-e83581df616f'
        ptsd_severe_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPTSD_severe.JPG?alt=media&token=f401ddf3-6e06-4567-8ee4-70eb0830b41f'

        phq9_none_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb'
        phq9_mild_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb'
        phq9_moderate_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb'
        phq9_moderatelysevere_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_moderatelysevere.JPG?alt=media&token=e8ea32d2-4b7f-4ed9-9470-9b8844a0851b'
        phq9_severe_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_severe.JPG?alt=media&token=fc8c741a-a3f5-4e95-9079-19e4bda0c6f3'

        ace_none_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FACE_none.JPG?alt=media&token=13c7dd04-b25b-49c1-85a3-14dc174dac7d'
        ace_moderate_img ='https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FACE_moderate.JPG?alt=media&token=cb316679-8418-48b9-923a-f54722c919a3'
        ace_severe_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FACE_severe.JPG?alt=media&token=d4407bed-7bdd-47be-96dc-58e74ca5eaed'

        # PTSD

        # if 0 <= user_scores['ptsdscore'] <= 5:
        #     PTSD = "None"
        #     PTSD_treatment = "None"
        #     ptsd_img = ptsd_none_img
        # elif 6 <= user_scores['ptsdscore'] <= 8:
        #     PTSD = "Mild"
        #     PTSD_treatment = "cognitive behavioral therapy (CBT), Prolonged Exposure Therapy, Eye Movement Desensitization and Reprocessing, Stress Inoculation Training"
        #     ptsd_img = ptsd_none_img
        # elif 9 <= user_scores['ptsdscore'] <= 11:
        #     PTSD = "Moderate"
        #     PTSD_treatment = "Active treatment with pharmacotherapy and/or psychotherapy (cognitive behavioral therapy (CBT), Prolonged Exposure Therapy, Eye Movement Desensitization and Reprocessing, Stress Inoculation Training)"
        #     ptsd_img = ptsd_moderate_img
        # elif 12 <= user_scores['ptsdscore'] <= 15:
        #     PTSD = "Severe"
        #     PTSD_treatment = "Active treatment with pharmacotherapy and/or psychotherapy (cognitive behavioral therapy (CBT), Prolonged Exposure Therapy, Eye Movement Desensitization and Reprocessing, Stress Inoculation Training)"
        #     ptsd_img = ptsd_severe_img

        # GAD7
        if 0 <= user_scores['gad7score'] <= 4:
            gad7 = "minimal anxiety"
            gad7_treatment = "none"

        elif 5 < user_scores['gad7score'] < 9:
            gad7 = "mild anxiety"
            gad7_treatment = "watchful waiting; repeat GAD-7 at follow-up"

        elif 10 <= user_scores['gad7score'] <= 14:
            gad7 = "moderate anxiety"
            gad7_treatment = "treatment plan, considering counseling, follow-up with either pharmacotherapy and/or psychotherapy"

        else:
            gad7 = "severe anxiety"
            gad7_treatment = "immediate initiation of pharmacotherapy and, if severe impairment or poor response to therapy, expedited referral to a mental health specialist for psychotherapy and/or collaborative management"

        # PHQ9

        if 0 <= user_scores['phq9score'] <= 4:
            phq9 = "None"
            phq9_treatment = "None"
            phq9_img = phq9_none_img
        elif 5 <= user_scores['phq9score'] <= 9:
            phq9 = "Mild"
            phq9_treatment = "Create a treatment plan, consider counseling, follow-up and/or pharmacotherapy"
            phq9_img = phq9_mild_img
        elif 10 <= user_scores['phq9score'] <= 14:
            phq9 = "Moderate"
            phq9_treatment = "Treatment plan, considering counseling, follow-up and/or pharmacotherapy"
            phq9_img = phq9_moderate_img

        elif 15 <= user_scores['phq9score'] <= 19:
            phq9 = "Moderately Severe"
            phq9_treatment = "Active treatment with pharmacotherapy and/or psychotherapy"
            phq9_img = phq9_moderatelysevere_img
        else:
            phq9 = "Severe"
            phq9_treatment = "Immediate initiation of pharmacotherapy and, if severe impairment or poor response to therapy, expedited referral to a mental health specialist for psychotherapy and/or collaborative management"
            phq9_img = phq9_severe_img

        # ACE

        if 0 <= user_scores['acescore'] <= 1:
            ace = "None"
            ace_treatment = "None"
            ace_img = ace_none_img
        elif 1 < user_scores['acescore'] < 4:
            ace = "Moderate"
            ace_treatment = "Provide education about toxis stress, its likely role in patient's health condition(s), and resilience. Assess for protective factors and jointly formulate treatment plan. Also, Link to support services and treatment as appropriate."
            ace_img = ace_moderate_img
        elif user_scores['acescore'] >= 4:
            ace = "Severe"
            ace_treatment = "Provide education about toxis stress, its likely role in patient's health condition(s), and resilience. Assess for protective factors and jointly formulate treatment plan. Also, link to support services and treatment as appropriate."
            ace_img = ace_severe_img

        # Therapist

        if user_scores['gad7score'] >= 5 or user_scores['phq9score'] >= 5 or user_scores['acescore'] > 0:
            therapist = random.choice(therapist_names)
        elif user_scores['gad7score'] < 5 and user_scores['phq9score'] < 5 and user_scores['acescore'] == 0:
            therapist = 'None'

        # creating a dataframe in which results will be stored
        data = {'GAD7_Score': user_scores['gad7score'], 'GAD7_Risk': gad7,
                'GAD7_Treatment_Options': gad7_treatment, 'PTSD_meter': "",
                'PHQ9_Score': user_scores['phq9score'], 'PHQ9_Risk': phq9,
                'PHQ9_Treatment_Options': phq9_treatment, 'PHQ9_meter': phq9_img,
                'ACE_Score': user_scores['acescore'], 'Ace_Risk': ace,'ACE_meter':ace_img,'ACE_Treatment_Options': ace_treatment ,'Therapist': therapist}

        # df = pd.DataFrame(data, orient='index')
        return jsonify(code="00", status="Success", result=data), 200

    except Exception as ex:
        return jsonify(code="02", msg="An error occurred: {}".format(str(ex))), 500


@app.route('/followup', methods=['POST'])
def secondary():
    try:
        therapist_names = ('Sarah', 'Mindy')
        filter_params = request.get_json()

        print('Variables', filter_params)

        user_scores = filter_params['query']

        phq9_none_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb'
        phq9_mild_img ='https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb'
        phq9_moderate_img = 'https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_none.JPG?alt=media&token=f6afecc1-61ae-48e0-bf1e-0f5b3d0d53bb'
        phq9_moderatelysevere_img ='https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_moderatelysevere.JPG?alt=media&token=e8ea32d2-4b7f-4ed9-9470-9b8844a0851b'
        phq9_severe_img ='https://firebasestorage.googleapis.com/v0/b/candlelit-ce011.appspot.com/o/Meters%2FPHQ9_severe.JPG?alt=media&token=fc8c741a-a3f5-4e95-9079-19e4bda0c6f3'

        # PHQ9

        if 0 <= user_scores['phq9score'] <= 4:
            phq9 = "None"
            phq9_treatment = "None"
            phq9_img = phq9_none_img

        elif 5 <= user_scores['phq9score'] <= 9:
            phq9 = "Mild"
            phq9_treatment = "Watchful waiting; repeat PHQ-9 at follow-up"
            phq9_img = phq9_mild_img

        elif 10 <= user_scores['phq9score'] <= 14:
            phq9 = "Moderate"
            phq9_treatment = "Treatment plan, considering counseling, follow-up and/or pharmacotherapy"
            phq9_img = phq9_moderate_img
        elif 15 <= user_scores['phq9score'] <= 19:
            phq9 = "Moderately Severe"
            phq9_treatment = "Active treatment with pharmacotherapy and/or psychotherapy"

            phq9_img = phq9_moderatelysevere_img
        else:
            phq9 = "Severe"
            phq9_treatment = "Immediate initiation of pharmacotherapy and, if severe impairment or poor response to therapy, expedited referral to a mental health specialist for psychotherapy and/or collaborative management"
            phq9_img = phq9_severe_img

        # GAD7
        if 0 <= user_scores['gad7score'] <= 4:
            gad7 = "minimal anxiety"
            gad7_treatment = "none"

        elif 5 < user_scores['gad7score'] < 9:
            gad7 = "mild anxiety"
            gad7_treatment = "watchful waiting; repeat GAD-7 at follow-up"

        elif 10 <= user_scores['gad7score'] <= 14:
            gad7 = "moderate anxiety"
            gad7_treatment = "treatment plan, considering counseling, follow-up with either pharmacotherapy and/or psychotherapy"

        else:
            gad7 = "severe anxiety"
            gad7_treatment = "immediate initiation of pharmacotherapy and, if severe impairment or poor response to therapy, expedited referral to a mental health specialist for psychotherapy and/or collaborative management"

            # Therapist
        if user_scores['phq9score'] >= 5 or user_scores['gad7score'] >= 5:
            therapist = random.choice(therapist_names)
        elif user_scores['phq9score'] < 5 and user_scores['gad7score'] < 5:
            therapist = 'None'

        data = {'PHQ9_Score': user_scores['phq9score'], 'PHQ9_Risk': phq9,
                'PHQ9_Treatment_Options': phq9_treatment, 'PHQ9_meter':phq9_img,
                'GAD7_Score': user_scores['gad7score'], 'GAD7_Risk': gad7, 'GAD7_Treatment_Options': gad7_treatment,
                'Therapist': therapist}

        return jsonify(code="00", status="Success", result=data), 200

    except Exception as ex:
        return jsonify(code="02", msg="An error occurred: {}".format(str(ex))), 500


if __name__ == '__main__':
    app.run(app.run(host=os.getenv('IP', '0.0.0.0'), port=int(os.getenv('PORT', 4444))))
